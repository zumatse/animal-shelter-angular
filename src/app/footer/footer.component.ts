import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  address = {
    street: '80692 Nolan Roads',
    city: 'Port Kuhlmanfurt',
    zipCode: '60272-7862'
  }
}
