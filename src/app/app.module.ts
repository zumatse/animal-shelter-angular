import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomepageComponent } from './homepage/homepage.component';
import { routing } from './app.routing';
import { GalleryComponent } from './gallery/gallery.component';
import { ModalComponent } from './modal/modal.component';
import { TableComponent } from './table/table.component';
import { SortableColumnComponent } from './table/sorttable/sortable-column/sortable-column.component';
import { SortService } from './table/sorttable/sort.service';
import { SortableTableDirective } from './table/sorttable/sortable-table.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomepageComponent,
    GalleryComponent,
    ModalComponent,
    TableComponent,
    SortableColumnComponent,
    SortableTableDirective,
  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [SortService],
  bootstrap: [AppComponent]
})
export class AppModule { }
