import { Routes, RouterModule } from "@angular/router";

import { HomepageComponent } from './homepage/homepage.component';
import { GalleryComponent } from './gallery/gallery.component';
import { TableComponent } from './table/table.component';



const APP_ROUTES: Routes = [
  {path: '', component: HomepageComponent },
  {path: 'pets', component: TableComponent},
  {path: 'gallery', component: GalleryComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTES);