import { Component, OnInit, ViewChild } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import {  NgForm } from '@angular/forms';
import 'rxjs/Rx';

import { Pet } from './pet.model';
import { Observable } from 'rxjs/Observable';
import { TableService } from './table.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [TableService]
})
export class TableComponent implements OnInit {
  pets:any = [];

  showForm: boolean = false;
  lastId: number;
  isEditted: boolean = false;
  currentPet: Pet = {
    id: 0,
    animal: '',
    name: '',
    age: 0,
    gender: ''
  }
  currentID: number;
  @ViewChild('form') editForm: NgForm;
  constructor(private http: Http, private tableService: TableService) { }

  showEditForm() {
    this.showForm = true;
  }

  hideEditForm() {
    this.editForm.reset();
    this.currentPet = {
      id: 0,
      animal: '',
      name: '',
      age: 0,
      gender: ''
    }
    this.isEditted = false;
    this.showForm = false;
  }

  onSubmit() {
    let form = this.editForm.value
    if (this.isEditted) {
      let pet = this.pets[this.currentID]
      pet.name = form.name;
      pet.age = form.age;
      pet.gender = form.gender;
      pet.animal = form.animal;
      this.tableService.updatePet(pet)
        .subscribe(
          data => console.log(data),
          error => console.error(error)
        )
    } else {
      this.pets.length ? this.lastId += 1 : this.lastId
      let newPet = new Pet(this.lastId, form.animal, form.gender, form.name, form.age);
      this.tableService.createPet(newPet)
        .subscribe(
          data => console.log(data),
          error => console.error(error)
        );
      this.pets.push(newPet);
    }
    this.hideEditForm();
  }

  getRowID(row: Event) {
    let el = <HTMLInputElement>row.target;
    return +el.closest('tr').getAttribute('id');
  }
  onEdit(row: Event) {
    this.isEditted = true;
    this.currentID = this.getRowID(row);
    this.currentPet = this.pets[this.currentID];
    this.showEditForm();
  }

  onDelete(row: Event) {
    let rowID = this.getRowID(row);
    this.currentID = this.pets[rowID].id
    this.pets.splice(rowID, 1);
    this.tableService.deletePet(this.currentID).subscribe(
      result => console.log(result)
    );
  }

  onSorted(event) {
    this.pets.sort((a,b) => {
      if (event.sortDirection === 'desc') {
        return a[event.sortColumn] < b[event.sortColumn];
      } else {
        return a[event.sortColumn] > b[event.sortColumn];
      }
    });
  }

  ngOnInit() {
    this.tableService.getPets().subscribe((pets: Pet[]) => {
      this.pets = pets;
      this.lastId = this.pets.length || 0;
    });
  }
}

export class PetSearchCriteria {
  sortColumn: string;
  sortDirection: string;
}