import { Http, Response, Headers } from "@angular/http";
import { Injectable, EventEmitter } from "@angular/core";
import 'rxjs/Rx';
import { Observable } from "rxjs";

import { Pet } from "./pet.model";

@Injectable()
export class TableService {
    private pets: Pet[] = [];

    constructor(private http: Http) {}

    getPets() {
      return this.http.get('http://localhost:3000/pets/data').map((response: Response) => {
        const pets = response.json();
        let transformedPets: Pet[] = [];
        for (let pet of pets ) {
          transformedPets.push(new Pet(pet.id, pet.animal, pet.gender, pet.name, pet.age));
        }
        this.pets = transformedPets;
        return transformedPets;
      })
      .catch((error: Response) => Observable.throw(error.json()));
    }

    createPet(pet) {
      const body = JSON.stringify(pet);
      const headers = new Headers({'Content-Type': 'application/json'});
      return this.http.post('http://localhost:3000/pets', body, {headers: headers}).map((response: Response) => {
        response.json();
      })
      .catch((error: Response) => Observable.throw(error.json()));
    }

    deletePet(id) {
      return this.http.delete('http://localhost:3000/pets/'+id)
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()));
    }

    updatePet(pet){
      const body = JSON.stringify(pet);
      const headers = new Headers({'Content-Type': 'application/json'});
      return this.http.patch('http://localhost:3000/pets/' + pet.id, body, {headers: headers})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
    }

}