export class Pet {
  constructor(public id:number, public animal:string, public gender:string, public name:string, public age:number) {}
}