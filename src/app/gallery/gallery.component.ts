import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { NgStyle } from '@angular/common';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  images: any = [];

  // Inject HttpClient into your component or service.
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    // Make the HTTP request:
    return this.http.get(`https://pixabay.com/api/?key=7539106-db48cda63bbc50f8bfc0ccac4&q=animals&image_type=photo&pretty=true&per_page=50`)
    .subscribe(data => {
      // Read the result field from the JSON response.
      this.images = data['hits'].map(el=> el.webformatURL);

      // console.log(this.images);
    });
  }

  setMyStyles(image) {
    let styles = {
      'backgroundImage': `url('${image}')`
    }
    return styles;
  }

}
