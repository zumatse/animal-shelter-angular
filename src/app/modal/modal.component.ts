import { Component, OnInit, Renderer2, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  public visible = false;
  @ViewChild('img') img: ElementRef;
  @ViewChild('modal') modal: ElementRef;

  constructor() { }

  ngOnInit() { }

  public show(event: MouseEvent): void {
    const target = <HTMLElement> event.target;
    const regexp = new RegExp(/(["'])(.*?[^\\])\1/);
    const url = target.getAttribute('style').match(regexp)[0].replace(/"/g, '');
    this.img.nativeElement.setAttribute('src', url);
    this.visible = true;
  }

  public hide(): void {
    this.visible = false;
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('backdrop')) {
      this.hide();
    }
  }
}
